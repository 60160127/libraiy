/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package database;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import libraryproject.LibraryProject;

/**
 *
 * @author UNKNOW
 */
public class UserDao {
    
    public static boolean insert(User user){
        Connection conn = database.connect();
        Statement stm = null;
        try {
            stm = conn.createStatement();
            String sql = "INSERT INTO User (\n" +
"                    logInName,\n" +
"                    password, \n" +
"                    name,\n" +
"                    surname,            \n" +
"                    typeId    \n" +
"                 )\n" +
"                 VALUES (\n" +
"                     '%s',\n" +
"                     '%s',\n" +
"                     '%s',\n" +
"                     '%s',\n" +
"                     %d, \n" +
"                 );";
            stm.execute(String.format(sql, user.getLoginName(),user.getPassword(),user.getName(),user.getSurname(),user.getTypeId()));
            database.close();
            return true;
        
        } catch (SQLException ex) {
            Logger.getLogger(UserDao.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        database.close();
        return true;
    }
    public static boolean update (User user){
        String sql = "UPDATE User  SET \n" +
                "  logInName = '%s',\n" +
                "  password = '%s',\n" +
                "  name = '%s',\n" +
                "  surname = '%s',\n" +
                "  typeId = %d\n" +
                "WHERE  userId = %d;";
        Connection conn = database.connect();
        Statement stm;
        try {
            stm = conn.createStatement();
            boolean ret = stm.execute(String.format(sql, 
                user.getLoginName(),
                user.getPassword(),
                user.getName(),
                user.getSurname(),
                user.getTypeId(),
                user.getUserId()
            ));
            database.close();
            return ret;
        } catch (SQLException ex) {
            Logger.getLogger(UserDao.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        return true;
    }
     public static boolean delete (User user){
         String sql ="DELETE FROM User  WHERE   userId = 1;";
         Connection conn =database.connect();
         
        try {
            Statement stm = conn.createStatement();
            boolean ret = stm.execute(String.format(sql, user.getUserId()));
            database.close();
            return ret;
        } catch (SQLException ex) {
            Logger.getLogger(UserDao.class.getName()).log(Level.SEVERE, null, ex);
        }
        
         database.close();
        return true;
    }
    public static ArrayList<User> getUsers(){
        ArrayList<User> list = new ArrayList();
        Connection conn = database.connect();
        try {
            Statement stm = conn.createStatement();
            String sql = "SELECT userId,\n" +
                        "       logInName,\n" +
                        "       password,\n" +
                        "       name,\n" +
                        "       surname,\n" +
                        "       typeId\n" +
                        "  FROM user";
            ResultSet rs = stm.executeQuery(sql);
            while(rs.next()){
                System.out.println(rs.getInt("userId") + " " + rs.getString("loginName"));
                User user =  toObject(rs);
                list.add(user);
                
        }
            database.close();
            return list;
        } catch (SQLException ex) {
            Logger.getLogger(LibraryProject.class.getName()).log(Level.SEVERE, null, ex);
        }
        database.close();
        return null;
    }

    private static User toObject(ResultSet rs) throws SQLException {
        User user;
        user = new User();
        user.setUserId(rs.getInt("userId"));
        user.setLoginName(rs.getString("loginName"));
        user.setName(rs.getString("name"));
        user.setSurname(rs.getString("surname"));
        user.setPassword(rs.getString("password"));
        user.setTypeId(rs.getInt("typeId"));
        return user;
    }
     public static User getUser(int userId){
         String sql = "SELECT * FROM user WHERE userId = %d";
         Connection conn = database.connect();
         
        try {
            Statement stm = conn.createStatement();
            ResultSet rs = stm.executeQuery(String.format(sql, userId));
            if(rs.next()){
               User user = toObject(rs); 
               database.close();
               return user;
            }
        } catch (SQLException ex) {
            Logger.getLogger(UserDao.class.getName()).log(Level.SEVERE, null, ex);
        }
        database.close();
        return null;
    }
    
}